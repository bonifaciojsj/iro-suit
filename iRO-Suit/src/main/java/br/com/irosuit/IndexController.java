package br.com.irosuit;

import br.com.irosuit.entity.Item;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author jose.bonifacio
 */
@Named
@ViewScoped
public class IndexController implements Serializable {

    private List<Item> items;

    public List<Item> getTest() {
        if (items == null) {
            try {
                Document doc = Jsoup.connect("http://ragial.com/search/iRO-Renewal/morrigane").get();
                Elements listLines = doc.select(".ilist tr");
                items = new ArrayList<>();
                for (Element line : listLines) {
                    Item item = new Item();
                    boolean isAvailable = false;
                    for (Element lineChildren : line.getAllElements()) {
                        if (lineChildren.className().equals("name")) {
                            for (Element nameColumnChildren : lineChildren.getAllElements()) {
                                if (nameColumnChildren.className().contains("activate_tr")) {
                                    isAvailable = true;
                                    break;
                                }
                            }
                            if (isAvailable) {
                                item.setName(lineChildren.text());
                            }
                        }
                        if (isAvailable) {
                            if (lineChildren.className().contains("price")) {
                                item.setValue(lineChildren.text());
                            }
                        }
                    }
                    items.add(item);
                }
            } catch (IOException ex) {
                Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return items;
    }
}
